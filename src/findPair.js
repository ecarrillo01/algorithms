// Given an array of integers arr and an integer k, create a boolean function
// that checks if there exist two elements in arr such that we get k when we add them together.

module.exports = function findPair(arr, k) {
    const visited = {}
    for (let item of arr) {
        if (visited[k - item]) {
            return true;
        }
        visited[item] = true;
    }
    return false;
}