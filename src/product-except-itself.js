// Given an array of integers, return a new array such that each element at index i of the new array is the product of all the numbers
// in the original array except the one at i.
// For example, if our input was [1, 2, 3, 4, 5], the expected output would be [120, 60, 40, 30, 24]. If our input was [3, 2, 1],
// the expected output would be [2, 3, 6].
// Follow-up: what if you can't use division?

// Dynamic programming solution
module.exports = function productExceptItself(arr) {
    if (arr.length === 1) return;
    const res = [];
    let prefix = 1;
    let suffix = 1
    for (let i = 0; i < arr.length; i++) {
        res[i] = prefix;
        prefix *= arr[i];
    }
    for (let i = arr.length - 1; i >= 0; i--) {
        res[i] *= suffix;
        suffix *= arr[i];
    }
    return res;
}
