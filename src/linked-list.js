module.exports.Node = class {
    constructor(data, next = null) {
        this.data = data;
        this.next = next;
    }
}

module.exports.LinkedList = class {
    constructor(head = null) {
        this.head = head;
    }

    reverse() {
        let current = this.head;
        let prev = null;
        let next = null;
        while (current) {
            next = current.next;
            current.next = prev;
            prev = current;
            current = next;
        }
        this.head = prev;
        return this;
    }

    forEach(fn) {
        let node = this.head;
        let cnt = 0;
        while (node) {
            fn(node, cnt);
            node = node.next;
            cnt++;
        }
    }

    toArray() {
        const arr = [];
        this.forEach((node) => {
            arr.push(node.data);
        });
        return arr;
    }
}

/* Basic idea: 
 * - Find the center point of the node list, using double steps (fast var) and 
 *   reverse the first half of the list.
 * - Walk on the second half and walk back in the first to the first difference.
 * - Return false if found diff, or true if all items are equal.
 */
exports.isPalindromeLinkedList = function (list) {
    let slow = null,
        fast = list,
        temp;
    // Find center point and reverse the first half of the list
    while (fast && fast.next) {
        fast = fast.next.next;
        temp = list.next;
        list.next = slow;
        slow = list;
        list = temp;
    }
    // If fast not null, list length is odd, ignore the center value
    if (fast) {
        list = list.next;
    }
    // Find the first difference
    while (list) {
        if (slow.data != list.data) return false;
        slow = slow.next;
        list = list.next;
    }
    // Return true, if no diff
    return true;
}
