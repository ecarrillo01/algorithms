function maxSubArray(arr) {
    let maxSum = arr[0];
    let currentSum = 0;
    for (let i = 0, max = arr.length; i < max; i++) {
        currentSum = Math.max(arr[i], currentSum + arr[i]);
        //compare maxSum with currentSum and store the greater value
        maxSum = Math.max(currentSum, maxSum);
        console.log(i);
    }
    return maxSum;
}


console.log(maxSubArray([
    -2,//0
    -3,//1
    4,//2
    - 1,//3
    -2,//4
    1,//5
    5,//6
    -3//7
]));
