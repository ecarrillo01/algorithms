module.exports = function longestSubstrWithoutRepeat(str) {
    const visited = {};
    let start = 0;
    let maxSubStr = 0;
    for (let i = start; i < str.length; i++) {
        if (visited[str[i]] >= start) {
            start = visited[str[i]] + 1;
        }
        visited[str[i]] = i;
        maxSubStr = Math.max(maxSubStr, i - start + 1);
    }
    return maxSubStr;
}