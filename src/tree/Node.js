module.exports = class Node {
    constructor(value, left = null, right = null) {
        this.value = value
        this.left = left;
        this.right = right
    }

    static getLastLeftNode(node) {
        if (!node.left) return node;
        return this.getLastLeftNode(node.left);
    }


    static getLastRightNode(node) {
        if (!node.right) return node;
        return this.getLastLeftNode(node.right);
    }

    static swapValues(node) {
        [node.left, node.right] = [node.right, node.left]
        if (node.left) this.swapValues(node.left);
        if (node.right) this.swapValues(node.right)
    }
}
