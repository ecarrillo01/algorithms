const Node = require('./Node');

const tree = new Node(4);
tree.left = new Node(6, new Node(3), new Node(5));
tree.right = new Node(7, new Node(1), new Node(2));
//            4
//         /     \
//        6       7
//       / \     / \
//      3   5   1   2

//Pre order traversal print
function preOrderTraversal(tree) {
    if (!tree) return;
    console.log(tree.value);
    preOrderTraversal(tree.left);
    preOrderTraversal(tree.right);
}

//In order traversal
function inOrderTraversal(root) {
    if (!root) return;
    inOrderTraversal(root.left);
    console.log(root.value);
    inOrderTraversal(root.right);
}

//Post order traversal
function postOrderTraversal(root) {
    if (!root) return;
    postOrderTraversal(root.left);
    postOrderTraversal(root.right);
    console.log(root.value);
}

exports.preOrderTraversal = preOrderTraversal;


//console.log(preOrderTraversal(tree));
//console.log(inOrderTraversal(tree));
// console.log(postOrderTraversal(tree))