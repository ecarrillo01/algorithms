const Node = require('./Node');
//const { preOrderTraversal } = require('./tree-dfs');

const root = new Node(6,
    new Node(8,
        new Node(2,
            new Node(7),
            new Node(3,
                new Node(10,
                    new Node(9, null, new Node(30))
                ),
                new Node(20, new Node(50))
            )
        )
    )
);
//preOrderTraversal(root);
function printNodes(node) {
    if (!node) return;
    console.log(node.value);
    printNodes(node.right);
    printNodes(node.left);
}
Node.swapValues(root);
printNodes(root);