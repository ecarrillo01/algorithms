// Given a string str, create a function that returns the first repeating character.
// If such character doesn't exist, return the null character '\0'.

module.exports = function firstRepeatingCharacter(str) {
    const visited = {};
    for (let item of str) {
        if (visited[item]) return item;
        visited[item] = true;
    }
    return "\0";
}