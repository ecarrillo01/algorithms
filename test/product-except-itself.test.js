const productExceptItself = require('../src/product-except-itself');

describe('Product except it self test suite', () => {
    it.each([
        { input: [10, 3, 5, 6, 2], expected: [180, 600, 360, 300, 900] },
        { input: [1, 2, 3, 4], expected: [24, 12, 8, 6] },
    ])('Should return the multiplication of all array items except itself', ({ input, expected }) => {
        const actual = productExceptItself(input);
        expect(actual).toEqual(expected);
    });
});