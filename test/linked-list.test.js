const { isPalindromeLinkedList, LinkedList, Node } = require('../src/linked-list');

describe('Palindrome linked list test suite', () => {
    it('Should return true for list with 3,4,4,3 values', () => {
        const list = new LinkedList(
            new Node(3,
                new Node(4,
                    new Node(4,
                        new Node(3)
                    )
                )
            )
        );
        const actual = isPalindromeLinkedList(list.head);
        expect(actual).toBeTruthy();
    })
});