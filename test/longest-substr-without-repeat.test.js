const longestSubstrWithoutRepeat = require('../src/longest-substr-without-repeat');

describe('Longest substring without repeating characters test suite', () => {
    it.each([
        { input: "abcdbeghef", expected: 6 },
        { input: "aaaaa", expected: 1 },
        { input: "eddy", expected: 2 },
    ])('Should return {expected} from input {input}', ({ input, expected }) => {
        const actual = longestSubstrWithoutRepeat(input);
        expect(actual).toEqual(expected);
    });
});