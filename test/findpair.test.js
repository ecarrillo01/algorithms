const findPair = require('../src/findPair');

describe('Find pair test suit with arr [4, 5, 1, -3, 6]', () => {
    const arr = [4, 5, 1, -3, 6];

    test('Using k = 11 should return true, since 4 + 5 is 11', () => {
        const actual = findPair(arr, 11);
        expect(actual).toBeTruthy();
    });

    test('Using k = -2 should return false, since -3 + 1 is 2', () => {
        const actual = findPair(arr, -2);
        expect(actual).toBeTruthy();
    });

    test('Using k = 8 should return false', () => {
        const actual = findPair(arr, 8);
        expect(actual).toBeFalsy();
    });

});