const firstRepeatingCharacter = require('../src/first-repeating-characters');

describe('First repeating character test suit', () => {
    it.each([
        { input: 'inside', expected: 'i' },
        { input: 'code', expected: '\0' },
        { input: 'programming', expected: 'r' },
        { input: 'abcd', expected: '\0' },
        { input: 'abba', expected: 'b' },
    ])('With input $input first repeating character should be $expected', ({ input, expected }) => {
        const actual = firstRepeatingCharacter(input);
        expect(actual).toBe(expected);
    });

});